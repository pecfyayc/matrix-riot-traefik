# Description

This is some code to run a local [Matrix](https://matrix.org/) installation including [Riot](https://about.riot.im/) for testing locally.

This code is intentionally not made for production usage and should not be used outside of `localhost`.

## Requirements

* [mkcert](https://github.com/FiloSottile/mkcert)
* [Docker Compose](https://docs.docker.com/compose/install/)

## Getting started

Let `mkcert` create a local CA:

```bash
mkcert -install
```

Create a certificate for `synapse.localhost` and `riot.localhost` which will be place in the correct folder to be consumed by `Traefik`:

```bash
mkcert -key-file certs/localhost-key.pem -cert-file certs/localhost.pem synapse.localhost riot.localhost
```

Start using `docker-compose`:

```bash
docker-compose up -d
```

and check the logs:

```bash
docker-compose logs
```
